package com.atlassian.http.mime;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static com.atlassian.http.mime.UserAgentUtil.BrowserFamily;
import static com.atlassian.http.mime.UserAgentUtil.Browser;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion;
import static com.atlassian.http.mime.UserAgentUtil.OperatingSystem;
import static com.atlassian.http.mime.UserAgentUtil.UserAgent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserAgentUtilImplTest
{
    public static final String IE_8_UA = "Mozilla/4.0 (compatible; MSIE 8.0" +
        "; Windows NT 6.0; Trident/4.0)";
    private static final String IE_7_UA = "Mozilla/4.0 (compatible; MSIE 7.0" +
            "; Windows NT 6.0)";
    // http://blogs.msdn.com/b/ieinternals/archive/2013/09/21/internet-explorer-11-user-agent-string-ua-string-sniffing-compatibility-with-gecko-webkit.aspx
    private static final String IE_10_UA = "Mozilla/5.0 (compatible; " +
        "MSIE 10.0; Windows NT 6.2; Trident/6.0)";
    private static final String IE_11_DEFAULT_UA = "Mozilla/5.0" +
        " (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko";
    private static final String MICROSOFT_EDGE_12_UA = "Mozilla/5.0 " +
        "(Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) " +
        "Chrome/42.0.2311.135 Safari/537.36 Edge/12.10136";
    private static final String MICROSOFT_EDGE_UNKNOWN_UA = "Mozilla/5.0 " +
        "(Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) " +
        "Chrome/42.0.2311.135 Safari/537.36 Edge/13.10136";
    private static final String CHROME_32_UA = "Mozilla/5.0 (Windows NT 6.2;" +
        " Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) " +
        "Chrome/32.0.1667.0 Safari/537.36";
    private static final String FIREFOX_3_6 = "Mozilla/5.0 (Windows NT 6.1;" +
        " Win64; x64; rv:3.6) Gecko/20100101 Firefox/3.6";
    private final UserAgentUtilImpl userAgentUtil = new UserAgentUtilImpl();

    @Test
    public void testGetUserAgentInfoIE7() {
        final UserAgent ua = userAgentUtil.getUserAgentInfo(IE_7_UA);
        assertUserAgentMatchedExpected(ua, BrowserFamily.MSIE,
                BrowserMajorVersion.MSIE7,
                OperatingSystem.OperatingSystemFamily.WINDOWS);
        assertThat(BrowserUtils.isIE8OrGreater(IE_7_UA), is(false));
    }

    @Test
    public void testGetUserAgentInfoIE8()
    {
        final UserAgent ua = userAgentUtil.getUserAgentInfo(IE_8_UA);
        assertUserAgentMatchedExpected(ua, BrowserFamily.MSIE,
            BrowserMajorVersion.MSIE8,
            OperatingSystem.OperatingSystemFamily.WINDOWS);
        assertThat(BrowserUtils.isIE8OrGreater(IE_8_UA), is(true));
    }

    @Test
    public void testGetUserAgentInfoIE10()
    {
        final UserAgent ua = userAgentUtil.getUserAgentInfo(IE_10_UA);
        assertUserAgentMatchedExpected(ua, BrowserFamily.MSIE,
            BrowserMajorVersion.MSIE10,
            OperatingSystem.OperatingSystemFamily.WINDOWS);
    }

    @Test
    public void testGetUserAgentInfoIE11()
    {
        final UserAgent ua = userAgentUtil.getUserAgentInfo(IE_11_DEFAULT_UA);
        assertUserAgentMatchedExpected(ua, BrowserFamily.MSIE_TRIDENT,
            BrowserMajorVersion.MSIE11,
            OperatingSystem.OperatingSystemFamily.WINDOWS);
        assertThat(BrowserUtils.isIE8OrGreater(IE_11_DEFAULT_UA), is(true));
    }

    @Test
    public void testIsIE()
    {
        for (String userAgent: Arrays.asList(IE_8_UA, IE_10_UA, IE_11_DEFAULT_UA))
        {
            assertThat(BrowserUtils.isIE(userAgent), is(true));
        }
    }

    @Test
    public void testGetUserAgentInfoMSEdge12()
    {
        final UserAgent ua = userAgentUtil.getUserAgentInfo(MICROSOFT_EDGE_12_UA);
        assertUserAgentMatchedExpected(ua, BrowserFamily.MS_EDGE,
            BrowserMajorVersion.MS_EDGE_12,
            OperatingSystem.OperatingSystemFamily.WINDOWS);
        assertThat(ua.getBrowser().getBrowserMinorVersion(), is("Edge/12.10136"));
        assertThat(BrowserUtils.isIE8OrGreater(MICROSOFT_EDGE_12_UA), is(false));
        assertThat(BrowserUtils.isIE(MICROSOFT_EDGE_12_UA), is(false));
    }

    @Test
    public void testGetUserAgentInfoMSEdgeUnknown()
    {
        final UserAgent ua = userAgentUtil.getUserAgentInfo(MICROSOFT_EDGE_UNKNOWN_UA);
        assertUserAgentMatchedExpected(ua, BrowserFamily.MS_EDGE,
            BrowserMajorVersion.MS_EDGE_UNKNOWN,
            OperatingSystem.OperatingSystemFamily.WINDOWS);
        assertThat(ua.getBrowser().getBrowserMinorVersion(), is("Edge/13.10136"));
    }

    @Test
    public void testGetUserAgentInfoChrome()
    {
        final UserAgent ua = userAgentUtil.getUserAgentInfo(CHROME_32_UA);
        assertUserAgentMatchedExpected(ua, BrowserFamily.CHROME,
            BrowserMajorVersion.CHROME_UNKNOWN,
            OperatingSystem.OperatingSystemFamily.WINDOWS);
    }

    @Test
    public void testGetUserAgentInfoFirefox()
    {
        final UserAgent ua = userAgentUtil.getUserAgentInfo(FIREFOX_3_6);
        assertUserAgentMatchedExpected(ua, BrowserFamily.FIREFOX,
            BrowserMajorVersion.FIREFOX36,
            OperatingSystem.OperatingSystemFamily.WINDOWS);
    }

    @Test
    public void testGetBrowserFamilyInfo() {
        for (BrowserFamily browserFamily : BrowserFamily.values())
        {
            final BrowserFamily actual = userAgentUtil.
                getBrowserFamily(browserFamily.getUserAgentString());
            assertThat(actual, is(browserFamily));
        }
    }

    private void assertUserAgentMatchedExpected(UserAgent userAgent,
                               BrowserFamily expectedFamily,
                               BrowserMajorVersion expectedBrowserMajorVersion,
                               OperatingSystem.OperatingSystemFamily
                                   expectedOsFamily)
    {
        final Browser browser = userAgent.getBrowser();
        assertThat(browser.getBrowserFamily(), is(expectedFamily));
        assertThat(browser.getBrowserMajorVersion(),
            is(expectedBrowserMajorVersion));
        assertEquals(userAgent.getOperatingSystem().getOperatingSystemFamily(),
            expectedOsFamily);
    }


}
