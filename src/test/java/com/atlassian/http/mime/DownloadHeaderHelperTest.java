package com.atlassian.http.mime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class DownloadHeaderHelperTest {
    private DownloadPolicyProvider mockPolicyProvider;
    private ContentDispositionHeaderGuesser contentDispositionGuesser;
    private HostileExtensionDetector hostileExtensionDetector;

    @BeforeEach
    public void setUp() throws Exception {
        hostileExtensionDetector = new HostileExtensionDetector();
        mockPolicyProvider = Mockito.mock(DownloadPolicyProvider.class);
        contentDispositionGuesser = new ContentDispositionHeaderGuesser(mockPolicyProvider, hostileExtensionDetector);
    }

    @Test
    public void testConstantHeaders() {
        DownloadHeaderHelper downloadHeaderHelper = new DownloadHeaderHelper("inline", "example.txt", "text/plain");
        HashMap <String, String> headers = downloadHeaderHelper.getDownloadHeaders();
        assertEquals("nosniff", headers.get("X-Content-Type-Options"));
        assertEquals("noopen", headers.get("X-Download-Options") );
    }

    @Test
    public void testDownloadHeaderHelperCanDetermineAppropriateContentDisposition() {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Smart);
        DownloadHeaderHelper downloadHeaderHelper = new DownloadHeaderHelper(contentDispositionGuesser, "example.txt", "text/plain", "chrome");
        assertEquals("inline", downloadHeaderHelper.getContentDisposition());
        assertEquals("text/plain", downloadHeaderHelper.getContentType());
        downloadHeaderHelper = new DownloadHeaderHelper(contentDispositionGuesser, "example.txt", "text/html", "chrome");
        assertEquals("attachment", downloadHeaderHelper.getContentDisposition());
        assertEquals("text/html", downloadHeaderHelper.getContentType());
    }
}
