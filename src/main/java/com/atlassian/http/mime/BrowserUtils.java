package com.atlassian.http.mime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.http.mime.UserAgentUtil.BrowserFamily.MSIE;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.MSIE7;


public class BrowserUtils {
    private static final Logger log = LoggerFactory.getLogger(BrowserUtils.class);

    public static boolean isIE8OrGreater(String userAgent) {
        if (StringUtils.isBlank(userAgent)) {
            return false;
        }
        final UserAgentUtil.Browser browser = getBrowserObject(userAgent);
        return isIE(userAgent) &&
            browser.getBrowserMajorVersion().compareTo(UserAgentUtil.BrowserMajorVersion.MSIE7) >= 1;
    }

    public static boolean isIE(String userAgent) {
        if (StringUtils.isBlank(userAgent)) {
            return false;
        }
        final UserAgentUtil userAgentUtil = new UserAgentUtilImpl();
        final UserAgentUtil.BrowserFamily family = userAgentUtil.getBrowserFamily(userAgent);
        return family == UserAgentUtil.BrowserFamily.MSIE ||
            family == UserAgentUtil.BrowserFamily.MSIE_TRIDENT;
    }

    private static UserAgentUtil.Browser getBrowserObject(String userAgent) {
        final UserAgentUtil userAgentUtil = new UserAgentUtilImpl();
        final UserAgentUtil.UserAgent userAgentInfo = userAgentUtil.getUserAgentInfo(userAgent);

        return userAgentInfo.getBrowser();
    }

}
