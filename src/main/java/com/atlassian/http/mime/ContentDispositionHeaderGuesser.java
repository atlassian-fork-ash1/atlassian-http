package com.atlassian.http.mime;

import com.google.common.annotations.VisibleForTesting;
import org.apache.tika.detect.TextDetector;
import org.apache.tika.mime.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * This class can sniff a file according to the current settings specified by the MimeSNiffPolicyProvider and determine
 * how raw files should be handled.
 */
public class ContentDispositionHeaderGuesser {
    private static final Logger log = LoggerFactory.getLogger(ContentDispositionHeaderGuesser.class);

    private static final String CONTENT_DISPOSITION_ATTACHMENT = "attachment";
    private static final String CONTENT_DISPOSITION_INLINE = "inline";

    private final DownloadPolicyProvider downloadPolicyProvider;
    private final HostileExtensionDetector hostileExtensionDetector;
    private final TextDetector textDetector;

    @VisibleForTesting
    ContentDispositionHeaderGuesser(DownloadPolicyProvider downloadPolicyProvider, HostileExtensionDetector hostileExtensionDetector, TextDetector textDetector) {
        this.downloadPolicyProvider = downloadPolicyProvider;
        this.hostileExtensionDetector = hostileExtensionDetector;
        this.textDetector = textDetector;
    }

    public ContentDispositionHeaderGuesser(DownloadPolicyProvider downloadPolicyProvider, HostileExtensionDetector hostileExtensionDetector) {
        this(downloadPolicyProvider, hostileExtensionDetector,
                new TextDetector());
    }

    /**
     * This will suggest a content disposition type (inline or attachment) for the given file, respecting the settings
     * given by the provided download policy.
     * <p>
     * Special treatment is given to html/xml - they will always be displayed inline if the contentType is set to
     * a text type, and if the browser is not IE.
     * </p>
     *
     * @param fileName the name of the file
     * @param mimeContentType the existing content type
     * @return either INLINE or ATTACHMENT ready for a Content-Disposition header
     */
    public String guessContentDispositionHeader(String fileName, String mimeContentType, String userAgent) {
        final DownloadPolicy downloadPolicy = downloadPolicyProvider.getPolicy();
        boolean forceDownload = false;

        if (downloadPolicy.equals(DownloadPolicy.Insecure) ) {
            // only in insecure mode we allow direct view of executable content
            return CONTENT_DISPOSITION_INLINE;
        }
        if (downloadPolicy.equals(DownloadPolicy.Secure) ||
                downloadPolicy.equals(DownloadPolicy.WhiteList)) {
            return CONTENT_DISPOSITION_ATTACHMENT;
        }

        forceDownload = isExecutableContent(fileName, mimeContentType);

        //allow inline view for browsers except IE, but only in smart mode
        if (BrowserUtils.isIE(userAgent) ) {
            /* text/plain is executable in IE */
            if (isTextContentType(mimeContentType)) {
                forceDownload = true;
            }
        }

        if (forceDownload && log.isDebugEnabled() ) {
            log.debug("\"" + fileName + "\" (" + mimeContentType + ")" + " presents as executable content, forcing download.");
        }

        return forceDownload ? CONTENT_DISPOSITION_ATTACHMENT : CONTENT_DISPOSITION_INLINE;
    }

    /**
     * Determines the appropriate headers to set in the response.
     * @param fileName the name of the file.
     * @param mimeContentType the existing content type.
     * @param userAgent the user agent making the request.
     * @param inputStream an input stream for the file which supports the mark feature.
     * @return
     */
    public Map<String, String> determineHeadersFromContent(
            final String fileName,
            String mimeContentType,
            final String userAgent,
            final InputStream inputStream) {
        final DownloadPolicy downloadPolicy = downloadPolicyProvider.getPolicy();
        boolean isIEBefore8 = BrowserUtils.isIE(userAgent) &&
                !BrowserUtils.isIE8OrGreater(userAgent);
        String contentDisposition;
        final boolean safeContentType = hostileExtensionDetector.isSafeContentType(
                mimeContentType);
        if (downloadPolicy.equals(DownloadPolicy.WhiteList)) {
            contentDisposition = safeContentType ? CONTENT_DISPOSITION_INLINE :
                    CONTENT_DISPOSITION_ATTACHMENT;
            if (isIEBefore8) {
                // No special treatment is given to IE versions < 8.
                contentDisposition = CONTENT_DISPOSITION_ATTACHMENT;
            }
            else if (!isBinary(inputStream)) {
                mimeContentType = "text/plain";
                contentDisposition = CONTENT_DISPOSITION_INLINE;
            }
            return new DownloadHeaderHelper(
                    contentDisposition, fileName, mimeContentType).getDownloadHeaders();
        }
        contentDisposition = guessContentDispositionHeader(fileName,
                mimeContentType, userAgent);
        if (downloadPolicy.equals(DownloadPolicy.Smart) &&
                hostileExtensionDetector.isTextContentType(mimeContentType) && !isIEBefore8) {
            contentDisposition = CONTENT_DISPOSITION_INLINE;
        }
        return new DownloadHeaderHelper(contentDisposition, fileName,
                mimeContentType).getDownloadHeaders();
    }

    /**
     * Determines if the inputstream contains binary content.
     *
     * @param inputStream an input stream for the file which supports the mark feature.
     * @return true if the given contentType is a binary file.
     */
    public boolean isBinary(final InputStream inputStream) {
        MediaType mediaType;
        try {
            mediaType = textDetector.detect(inputStream, null);
            return mediaType.equals(MediaType.OCTET_STREAM);
        } catch (IOException e) {
            return true;
        }
    }

    /**
     * @deprecated since 0.0.8 use {@link ContentDispositionHeaderGuesser#guessMIME(String, boolean)} instead
     * Given a filename, its MIME type, and a browser user agent, return a 'safe' mime type, depending on
     * the download policy configured, such that 'safe' browsers can display the executable text file without causing xss issues.
     * <p>
     * If the download policy is set to {@link DownloadPolicy#Secure} or {@link DownloadPolicy#Insecure}, then
     * the given mimeContentType will be returned.
     * </p><p>
     * If the download policy is set to {@link DownloadPolicy#Smart}, and the user agent is not Internet Explorer
     * , then for text files that can be displayed in  plain text (e.g., text/html, as determined by
     * {@link HostileExtensionDetector#isTextContentType(String)} and {@link HostileExtensionDetector#isTextExtension(String)}),
     * a 'text/plain' mime type will be returned instead of the given mimeContentType.
     * </p>
     *
     * @param filename the file to display
     * @param mimeContentType the mime type for the given file
     * @param userAgent the browser user agent.
     * @return a mime type
     */
    public String guessMIME(String filename, String mimeContentType, String userAgent) {
        if (CONTENT_DISPOSITION_INLINE.equals(guessContentDispositionHeader(filename, mimeContentType, userAgent))
                && isAllowInlineOverride(filename, mimeContentType, userAgent, downloadPolicyProvider.getPolicy())) {
            return "text/plain";
        } else {
            return mimeContentType;
        }
    }

    /**
     * Given a mimeContentType and whether the file is a binary file or not
     * returns text/plain for non-binary files
     * and the given mimeContentType is returned.
     * <p>
     * If the download policy is set to {@link DownloadPolicy#Secure} or {@link DownloadPolicy#Insecure}, then
     * the given mimeContentType will be returned.
     * </p><p>
     *
     * It is recommended to use the 'X-Content-Type-Options' header with a value of 'nosniff'
     * to avoid XSS issues in IE.
     * </p>
     * @param mimeContentType the the mime type for the given file
     * @param isBinaryFile if the file is a binary file or not (true iff binary)
     * @return a mime type
     */
    public String guessMIME(String mimeContentType, boolean isBinaryFile)
    {
        boolean inSmartMode = downloadPolicyProvider.getPolicy().equals(DownloadPolicy.Smart);
        if (!inSmartMode || isBinaryFile)
        {
            return mimeContentType;
        }
        return "text/plain";
    }

    private boolean isAllowInlineOverride(String fileName, String mimeContentType, String userAgent, DownloadPolicy downloadPolicy) {
        return downloadPolicy == DownloadPolicy.Smart
                && !BrowserUtils.isIE(userAgent)
                && isTextContentType(mimeContentType)
                && !StringUtils.isBlank(mimeContentType);
    }

    private boolean isTextContentType(String mimeContentType) {
        return hostileExtensionDetector.isTextContentType(mimeContentType);
    }

    private boolean isExecutableContent(String name, String mimeContentType) {
        return hostileExtensionDetector.isExecutableContent(name, mimeContentType);
    }
}
